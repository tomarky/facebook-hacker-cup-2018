import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	static final int MODULO = 1_000_000_007;
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			int N = scanner.nextInt();
			int[][] dp = new int[2][3];
			String[] grid = new String[3];
			for (int j = 0; j < 3; j++)
			{
				grid[j] = scanner.next();
			}
			dp[1][0] = 1;
			for (int j = 0; j < N; j++)
			{
				int p = j & 1;
				for (int k = 0; k < 3; k++)
				{
					dp[p][k] = 0;
				}
				if (grid[0].charAt(j) == '.' && grid[1].charAt(j) == '.')
				{
					dp[p][0] = dp[1 - p][1];
					dp[p][1] = dp[1 - p][0];
				}
				if (grid[1].charAt(j) == '.' && grid[2].charAt(j) == '.')
				{
					dp[p][1] = (dp[p][1] + dp[1 - p][2]) % MODULO;
					dp[p][2] = dp[1 - p][1];
				}
			}
			output(i, Integer.toString(dp[1 - (N & 1)][2]));
		}
	}
}