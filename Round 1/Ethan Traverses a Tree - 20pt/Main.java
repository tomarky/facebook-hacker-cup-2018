import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			int n = scanner.nextInt();
			int k = scanner.nextInt();
			int[] lTree = new int[n + 1];
			int[] rTree = new int[n + 1];
			int[] parent = new int[n + 1];
			for (int j = 1; j <= n; j++)
			{
				lTree[j] = scanner.nextInt();
				rTree[j] = scanner.nextInt();
				parent[lTree[j]] = j;
				parent[rTree[j]] = j;
			}
			parent[0] = 0;
			int[] preFlag = new int[n + 1];
			int[] postFlag = new int[n + 1];
			int preCur = 1, postCur = 1;
			UnionFindTree uft = new UnionFindTree(n);
			for (;;)
			{
				preCur = nextPreTraverse(lTree, rTree, parent, preFlag, preCur);
				postCur = nextPostTraverse(lTree, rTree, parent, postFlag, postCur);
				if (preCur == 0)
				{
					break;
				}
				uft.union(preCur, postCur);
			}
			int[] groups = uft.getGroups();
			if (groups[0] < k)
			{
				output(i, "Impossible");
			}
			else
			{
				StringBuilder sb = new StringBuilder();
				for (int j = 1; j <= n; j++)
				{
					if (j > 1) { sb.append(' '); }
					sb.append(groups[j] % k + 1);
				}
				output(i, sb.toString());
			}
		}
	}
	static int nextPreTraverse(int[] lTree, int[] rTree, int[] parent, int[] flag, int cur)
	{
		while (cur != 0)
		{
			switch (flag[cur])
			{
			case 0:
				flag[cur] = 1;
				return cur;
			case 1:
				flag[cur] = 2;
				if (lTree[cur] != 0)
				{
					cur = lTree[cur];
				}
				break;
			case 2:
				flag[cur] = 3;
				if (rTree[cur] != 0)
				{
					cur = rTree[cur];
				}
				break;
			case 3:
				flag[cur] = 4;
				cur = parent[cur];
				break;
			case 4:
				return 0;
			}
		}
		return 0;
	}
	static int nextPostTraverse(int[] lTree, int[] rTree, int[] parent, int[] flag, int cur)
	{
		while (cur != 0)
		{
			switch (flag[cur])
			{
			case 0:
				flag[cur] = 1;
				if (lTree[cur] != 0)
				{
					cur = lTree[cur];
				}
				break;
			case 1:
				flag[cur] = 2;
				if (rTree[cur] != 0)
				{
					cur = rTree[cur];
				}
				break;
			case 2:
				flag[cur] = 3;
				return cur;
			case 3:
				flag[cur] = 4;
				cur = parent[cur];
				break;
			case 4:
				return 0;
			}
		}
		return 0;
	}
}

class UnionFindTree
{
	int n, gid = 0;
	int[] group, node;
	UnionFindTree(int n)
	{
		this.n = n;
		group = new int[n + 1];
		node = new int[n + 1];
	}
	void union(int a, int b)
	{
		int ga = get(a);
		int gb = get(b);
		int g = select(ga, gb);
		node[a] = g;
		node[b] = g;
	}
	int[] getGroups()
	{
		int[] ret = new int[n + 1];
		int[] tmp = new int[n + 1];
		int id = 0;
		for (int i = 1; i <= n; i++)
		{
			int g = get(i);
			if (tmp[g] == 0)
			{
				id++;
				tmp[g] = id;
			}
			ret[i] = tmp[g];
		}
		ret[0] = id;
		return ret;
	}
	int select(int ga, int gb)
	{
		if (ga == 0)
		{
			if (gb == 0)
			{
				gid++;
				group[gid] = gid;
				return gid;
			}
			return gb;
		}
		if (gb == 0)
		{
			return ga;
		}
		if (ga < gb)
		{
			group[gb] = ga;
			return ga;
		}
		else
		{
			group[ga] = gb;
			return gb;
		}
	}
	int get(int x)
	{
		int g = node[x];
		while (g != group[g])
		{
			g = group[g];
		}
		return node[x] = g;
	}
}
