Facebook Hacker Cup 2018に参加したときのコード

Qualification Round 2018/07/06 ～ 2018/07/09 - 100pt 1129位 (144:20:43)
    Tourist - 25pt OK
    Interception - 30pt OK
    Ethan Searches for a String - 45pt OK
https://www.facebook.com/hackercup/scoreboard/838198113036494/?filter=everyone&offset=1100  


Round 1 2018/07/22 ～ 2018/07/23 - 35pt 984位 (7:44:28)
    Let It Flow - 15pt OK
    Ethan Traverses a Tree - 20pt OK
    Platform Parkour - 28pt non try
    Evening of the Living Dead - 37pt non try
https://www.facebook.com/hackercup/scoreboard/1825345887684301/?filter=everyone&offset=950


Round 2 2018/08/05 02:00 ～ 05:00 - 0pt 1009位
    Ethan Finds the Shortest Path - 8pt NG
    Jack's Candy Shop - 20pt Expired
    Replay Value - 32pt read but not try
    Fossil Fuels - 40pt read but not try
https://www.facebook.com/hackercup/scoreboard/211051912693116/?filter=everyone&offset=1000