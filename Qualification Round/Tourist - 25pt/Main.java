import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			int N = scanner.nextInt();
			int K = scanner.nextInt();
			long V = scanner.nextLong();
			String[] vs = new String[N];
			for (int j = 0; j < N; j++)
			{
				vs[j] = scanner.next();
			}
			int p = (int)(((V - 1L) * (long)K) % (long)N);
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < N; j++)
			{
				if (j + N < p + K || (p <= j && j < p + K))
				{
					if (sb.length() > 0)
					{
						sb.append(' ');
					}
					sb.append(vs[j]);
				}
			}
			output(i, sb.toString());
		}
	}
}