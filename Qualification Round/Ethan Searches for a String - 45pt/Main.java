import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			String answer = "Impossible";
			String A = scanner.next();
			if (A.length() > 1)
			{
				int h = (int)A.charAt(0);
				int p = 0;
				while ((p = A.indexOf(h, p + 1)) > 0)
				{
					String tail = A.substring(p);
					if (!A.startsWith(tail))
					{
						answer = A.substring(0, p) + A;
						break;
					}
				}
			}
			output(i, answer);
		}
	}
}