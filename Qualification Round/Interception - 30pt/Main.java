import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			int N = scanner.nextInt();
			for (int j = 0; j < N + 1; j++)
			{
				scanner.nextInt();
			}
			if (N % 2 == 0)
			{
				output(i, "0");
			}
			else
			{
				output(i, "1\n0.0");
			}
		}
	}
}

/*
	N = 0
	P(0)*x^0
	(P(0)*x)^0
	= 1
	answer is 0
	
	N = 1
	P(1)*x^1+P(0)*x^0
	(P(1)*x)^(((1+P(0))*x)^0)
	         ~~~~~~~~~~~~~~~~
	            always 1
	P(1)*x
	~~~~~~
	 = 0 ?
	P(1)!=0
	-> x = 0
	
	answer is 1 and 0.0
	
	N = 2
	P(2)*x^2+P(1)*x^1+P(0)*x^0
	(P(2)*x)^(((2+P(1))*x)^(((1+P(0))*x)^0))
	                       ~~~~~~~~~~~~~~~~
						      always 1
	(P(2)*x)^((2+P(1))*x)
	~~~~~~~~ ~~~~~~~~~~~~
	  = 0 ?     != 0 ?
	P(2)!=0
	-> x = 0 -> ((2+P(1))*0 = 0
	
	answer is 0
	
	N = 3
	P(3)*x^3+P(2)*x^2+P(1)*x^1+P(0)*x^0
	(P(3)*x)^(((3+P(2))*x)^(((2+P(1))*x)^(((1+P(0))*x)^0)))
	                                     ~~~~~~~~~~~~~~~~
										    always 1
	(P(3)*x)^(((3+P(2))*x)^(((2+P(1))*x)))
	Q(i) := (i+1)+P(i)
	(P(3)*x)^((Q(2)*x)^(Q(1)*x))
	~~~~~~~~ ~~~~~~~~~~~~~~~~~~~
	  = 0 ?          != 0 ?
	P(3)!=0
	-> x = 0  -> (Q(2)*0)^(Q(1)*0)
	               = 0^0 = 1
	answer is 1 and 0.0
	
	N = 4
	P(4)*x^4+P(3)*x^3+P(2)*x^2+P(1)*x^1+P(0)*x^0
	(P(4)*x)^(((4+P(3))*x)^(((3+P(2))*x)^(((2+P(1))*x)^(((1+P(0))*x)^0))))
	                                                   ~~~~~~~~~~~~~~~~
										                  always 1
	(P(4)*x)^(((4+P(3))*x)^(((3+P(2))*x)^((2+P(1))*x)))
	Q(i) := (i+1)+P(i)
	(P(4)*x)^((Q(3)*x)^((Q(2)*x)^(Q(1)*x)))
	~~~~~~~~ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	  = 0 ?           != 0 ?
	P(4)!=0
	-> x = 0  -> (Q(4)*0)^((Q(2)*0)^(Q(1)*0))
	                = 0^(0^0) = 0^1 = 0
	answer is 0
	
	N = 5
	0^(0^(0^0)) = 0^(0^1) = 0^0 = 1 != 0
	answer is 1 0.0
	
	N = 6
	0^(0^(0^(0^0))) = 0^(0^(0^1)) = 0^(0^0) = 0^1 = 0
	answer is 0
	
*/