import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			StringBuilder sb = new StringBuilder();
			int N = scanner.nextInt();
			int K = scanner.nextInt();
			
			if (N == 2)
			{
				sb.append("0\n");
				sb.append("1\n");
				sb.append("1 2 1");
			}
			else
			{
				int ans = 1 + K - 2;
				sb.append(ans);
				sb.append("\n");
				sb.append("3\n");
				sb.append("1 2 1\n");
				sb.append("1 " + N + " 2\n");
				sb.append("2 " + N + " " + K);
			}
			
			output(i, sb.toString());
		}
	}
}