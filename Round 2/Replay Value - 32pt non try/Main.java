import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			int n = scanner.nextInt();
			long s = scanner.nextLong();
			long e = scanner.nextLong();
			long[] xs = new long[n];
			long[] ys = new long[n];
			for (int j = 0; j < n; j++)
			{
				xs[j] = scanner.nextLong();
				ys[j] = scanner.nextLong();
			}
			long ans = 0L;
			
			for (int j = 0; j < n; j++)
			{
				
			}
			
			output(i, Long.toString(ans));
		}
	}
}

/*

2個のレーザーでスタート地点orゴール地点を囲むようにレーザーをクロスすれば達成できる

スタート地点にX座標で近いレーザーAがY軸スタート地点方向に
もう一方がスタート地点を囲むようにx軸方向にレーザー(クロスするにはY座標がスタート地点とレーザーAの中間にないとダメ)

この取り方の場合、他のレーザーの向きを考慮すると、重複するパターンを除かなければならない


逆に、到達可能なレーザー構成を考えて4^Nから引いて求めるという考え方もある

レーザーの座標は重複しないため、全部縦方向もしくは全部横方向、というのがOKパターンの１つ
縦横が混ざるパターンは難しい
要するにスタート地点かゴール地点を囲う向きのレーザーが発生しないことが条件となる





*/