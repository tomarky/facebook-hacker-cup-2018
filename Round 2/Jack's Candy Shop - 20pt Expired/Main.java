import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int i = 0; i < T; i++)
		{
			int N = scanner.nextInt();
			int M = scanner.nextInt();
			int A = scanner.nextInt();
			int B = scanner.nextInt();
			List<ArrayList<Integer>> tree = new ArrayList<>(N);
			for (int j = 0; j < N; j++)
			{
				tree.add(new ArrayList<Integer>());
			}
			int[] Ps = new int[N];
			for (int j = 1; j < N; j++)
			{
				int p = scanner.nextInt();
				tree.get(p).add(j);
				Ps[j] = p;
			}
			for (int j = 0; j < N; j++)
			{
				Collections.sort(tree.get(j));
			}
			// showTree(tree);
			int[] needs = new int[N];
			for (int j = 0; j < M; j++)
			{
				int c = (A * j + B) % N;
				needs[c]++;
			}
			List<ArrayList<Integer>> candies = new ArrayList<>(N);
			for (int j = 0; j < N; j++)
			{
				candies.add(new ArrayList<Integer>());
				candies.get(j).add(j);
			}
			long ans = 0L;
			List<List<Integer>> depth = getDepth(tree);
			Collections.reverse(depth);
			for (List<Integer> ds : depth)
			{
				for (int e : ds)
				{
					List<Integer> cs = candies.get(e);
					Collections.sort(cs);
					for (int j = 0; j < needs[e]; j++)
					{
						int z = cs.size() - 1;
						ans += (long)cs.get(z);
						cs.remove(z);
						if (cs.isEmpty()) { break; }
					}
					candies.get(Ps[e]).addAll(cs);
				}
			}
			output(i, Long.toString(ans));
			System.gc();
		}
	}
	static List<List<Integer>> getDepth(List<ArrayList<Integer>> tree)
	{
		List<List<Integer>> ret = new ArrayList<>(tree.size());
		List<Integer> nexts = new ArrayList<>();
		nexts.addAll(tree.get(0));
		ret.add(Arrays.asList(0));
		while (!nexts.isEmpty())
		{
			ret.add(nexts);
			List<Integer> temp = new ArrayList<>();
			for (Integer n : nexts)
			{
				temp.addAll(tree.get(n));
			}
			nexts = temp;
		}
		return ret;
	}
	static void showTree(List<ArrayList<Integer>> tree)
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("0\n");
		List<Integer> nexts = new ArrayList<>();
		for (Integer c : tree.get(0))
		{
			sb.append("0-" + c + " ");
		}
		sb.append("\n");
		nexts.addAll(tree.get(0));
		while (!nexts.isEmpty())
		{
			List<Integer> temp = new ArrayList<>();
			for (Integer n : nexts)
			{
				for (Integer c : tree.get(n))
				{
					sb.append(n + "-" + c + " ");
				}
				temp.addAll(tree.get(n));
			}
			sb.append("\n");
			nexts = temp;
		}
		System.out.println(sb.toString());
	}
}